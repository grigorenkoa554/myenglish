import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from '../app/pages/welcome/welcome.component';
import { UsersComponent } from '../app/pages/users/users.component';
import { LearnWordByCardComponent } from './pages/learn-word-by-card/learn-word-by-card.component';
import { EditTopicsComponent } from './pages/edit/edit-topics/edit-topics.component';
import { EditTopicComponent } from './pages/edit/edit-topic/edit-topic.component';
import { LoginComponent } from './account/account/login/login.component';
import { LogoutComponent } from './account/account/logout/logout.component';
import { RegisterComponent } from './account/account/register/register.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'users', component: UsersComponent },
  { path: 'learnWordByCard', component: LearnWordByCardComponent },
  { path: 'editTopics', component: EditTopicsComponent },
  { path: 'editTopic/:topicId', component: EditTopicComponent },
  { path: 'account/login', component: LoginComponent },
  { path: 'account/logout', component: LogoutComponent },
  { path: 'account/register', component: RegisterComponent },
  //{ path: '', pathMatch: 'full', redirectTo: '/welcome' },
  //{ path: 'welcome', component: WelcomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
