import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-back-item',
  templateUrl: './back-item.component.html',
  styleUrls: ['./back-item.component.less']
})
export class BackItemComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  backClicked() {
    this.location.back();
  }

}
