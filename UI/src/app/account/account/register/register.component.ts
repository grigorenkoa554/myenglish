import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
//import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { AccountService } from '../../account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {

  isRegistered = false;
  validateForm: FormGroup;
  //captchaTooltipIcon: NzFormTooltipIcon = {
  //  type: 'info-circle',
  //  theme: 'twotone'
  //};

  constructor(private fb: FormBuilder, private accountService: AccountService) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
      nickname: [null, [Validators.required]]
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (!this.validateForm.valid) {
      return;
    }

    const email = this.validateForm.value['email'];
    const password = this.validateForm.value['password'];
    const checkPassword = this.validateForm.value['checkPassword'];
    const nickname = this.validateForm.value['nickname'];
    this.accountService.register(email, password, checkPassword, nickname)
      .subscribe(() => {
        this.isRegistered = true;
        this.accountService.login(email, password)
          .subscribe();
      });
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  getCaptcha(e: MouseEvent): void {
    e.preventDefault();
  }

}
