export class LoginModel {
  email: string;
  password: string;
}

export class RegisterModel {
  email: string;
  password: string;
  checkPassword: string;
  nickname: string;
}

export class LoginResult {
  nickname: string;
}
