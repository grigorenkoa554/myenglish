import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Topic } from '../../model';
import { TopicService } from '../../services/topic.service';

@Component({
  selector: 'app-edit-topics',
  templateUrl: './edit-topics.component.html',
  styleUrls: ['./edit-topics.component.less']
})
export class EditTopicsComponent implements OnInit {
  public topics: Topic[] = [];
  isVisible = false;

  constructor(private router: Router, private topicService: TopicService,
    private nzMessageService: NzMessageService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.topicService.getTopics()
      .subscribe((data) => {
        this.topics = data;
      });
  }

  addTopic(newNameOfTopic: string) {
    this.topicService.addTopic(newNameOfTopic).subscribe(() => {
      this.init();
    });
    this.isVisible = false;
  }

  goToEditTopic(topicId: Guid) {
    this.router.navigate(['/editTopic', topicId]);
  }

  deleteTopic(id: Guid) {
    this.topicService.deleteTopic(id).subscribe(() => {
      this.init();
    });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }
  cancel(): void {
    this.nzMessageService.info('click cancel');
  }
}
