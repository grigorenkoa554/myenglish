import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Topic, Word, WordView } from '../../model';
import { TopicService } from '../../services/topic.service';
import { WordService } from '../../services/word.service';
//import * as _ from 'lodash';
import * as _ from 'underscore';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-edit-topic',
  templateUrl: './edit-topic.component.html',
  styleUrls: ['./edit-topic.component.less']
})
export class EditTopicComponent implements OnInit {
  topicId: Guid;
  topic: Topic;
  words: WordView[] = [];
  isVisible = false;
  isSelect = false;

  constructor(private topicService: TopicService,
    private wordService: WordService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.init();
  }

  init() {
    var textId = this.activatedRoute.snapshot.paramMap.get('topicId');
    this.topicId = Guid.parse(textId);
    this.topicService.getTopic(this.topicId).subscribe((data) => {
      this.topic = data;
      let sorted = _.sortBy(data.words, 'wordEng');
      this.words = sorted.map<WordView>(function (x) {
        let model: WordView = x;
        x.isSelected = false;
        return model;
      });
    });
  }

  saveNameOfTopic(name: string) {
    this.topicService.editTopic(this.topicId, name).subscribe(() => {
      this.init();
    });
  }

  saveEditedWord(wordId: Guid, wordEng: string, wordRus:
    string, partOfSpeech: number
  ) {
    this.wordService.editWord(wordId, wordEng, wordRus, partOfSpeech).subscribe(() => {
      this.init();
    });
  }

  playClick(url: string): void {
    this.wordService.playAudio(url);
  }

  addWord(wordEng: string, wordRus: string, partOfSpeech: number) {
    this.wordService.addWord(this.topicId, wordEng, wordRus, partOfSpeech).subscribe(() => {
      this.init();
    });
    this.isVisible = false;
  }

  deleteWord(id: Guid) {
    this.wordService.deleteWord(id).subscribe(() => {
      this.init();
    });
  }

  deleteWords(): void {
    let toDeleteWords = this.words.filter(x => x.isSelected);
    for (let word of toDeleteWords) {
      this.deleteWord(word.id);
    }
    this.isVisible = false;
    this.isSelect = false;
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  showCheckBox(): void {
    this.isSelect = true;
  }

  Cancel(): void {
    this.isVisible = false;
    this.isSelect = false;
  }
}
