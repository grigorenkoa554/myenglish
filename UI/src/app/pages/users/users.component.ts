import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Component({
  selector: 'app-user-path',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  arr1: number[] = [12, 12, 12, 12];
  firstName: string = 'Ivan';
  lastName: string = 'Ivanov';
  age: number = 23;
  login: string = 'someLogin';
  editUserModel: User = null;
  globalId: number = 1;
  error: string = '';
  someColor: string = 'red';
  backGroundColor: string = 'white';
  @Input() someInput: string = '';
  someCounter: number = 0;
  someString2: string = '';
  

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.init();
  }

  someClick(si: User): void {
    si.age++;
  }

  init() {
    this.userService.getUsers()
      .subscribe((data) => {
        this.users = data;
      });
  }

  addUser(firstName: string, lastName: string, age: number, login: string) {
    this.userService.addUser(firstName, lastName, age, login).subscribe(() => {
      this.init();
    });
  }

  deleteUser(user: User): void {
    this.userService.deleteUser(user.id).subscribe(() => this.init());
  }

  editUser(user: User): void {
    this.editUserModel = { ...user };
  }

  saveUser(): void {
    this.userService.editUser(this.editUserModel.id, this.editUserModel.firstName,
      this.editUserModel.lastName, this.editUserModel.age, this.editUserModel.login)
      .subscribe(() => {
        this.init();
      });
  }

  cancelUser(): void {
    this.editUserModel = null;
  }

  incrementSomeCounter(someString: string): void {
    this.someCounter++;
    this.someString2 += someString
  }

  anotherColor(stringColor: string): void {
    this.someColor = stringColor;
  }
  backgroundColor(color: string): void {
    this.backGroundColor = color;
  }
}

export class UserResult {
  data: string;
  descrition: string;
}

export class User {
  id: number;
  firstName: string;
  lastName: string;
  login: string;
  age: number;
}
