import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './users.component';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  host = '';

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.host + '/api/users');
  }

  addUser(firstName: string, lastName: string, age: number, login: string):
    Observable<User> {
    const user = { firstName, lastName, age, login };
    return this.http.post<User>(this.host + '/api/users', user);
  }

  editUser(id: number, firstName: string, lastName: string, age: number, login: string):
    Observable<string> {
    const user = { id, firstName, lastName, age, login };
    return this.http.put<string>(this.host + `/api/users/${id}`, user);
  }

  deleteUser(id: number): Observable<string> {
    return this.http.delete<string>(this.host + `/api/users/${id}`);
  }
}
