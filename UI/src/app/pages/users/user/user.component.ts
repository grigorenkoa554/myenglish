import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../users.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class UserComponent implements OnInit {
  @Input() public user: User;
  @Output() onChanged = new EventEmitter<string>();
  @Output() onChangedForDouble = new EventEmitter<string>();
  @Output() onChangedPink = new EventEmitter<string>();
  @Output() onChangedBlue = new EventEmitter<string>();
  @Output() onChangedBear = new EventEmitter<string>();

  constructor(private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }


  ngOnInit() {
  }

  clickForPink(): void {
    this.onChangedPink.emit("pink");
  }

  clickForBlue(): void {
    this.onChangedBlue.emit("lightblue");
  }
  clickBear(): void {
    this.onChangedBear.emit("blueviolet");
  }

  myClick(): void {
    this.onChanged.emit(this.user.firstName);
  }

  myDoubleClick(): void {
    this.onChangedForDouble.emit(this.user.lastName);
  }
 
}
