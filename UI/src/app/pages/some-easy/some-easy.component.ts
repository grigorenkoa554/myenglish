import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-some-easy',
  templateUrl: './some-easy.component.html',
  styleUrls: ['./some-easy.component.less']
})
export class SomeEasyComponent implements OnInit {
  @Input() myInput: string = '';
  @Input() mySecondInput: number = 0;

  constructor() { }

  ngOnInit() {
  }

}
