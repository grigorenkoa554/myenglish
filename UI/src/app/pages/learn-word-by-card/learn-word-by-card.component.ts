import { Component, OnInit } from '@angular/core';
import { Router, ParamMap } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { StartLearnParams, Word, KEY_CODE, TypeOfLearn } from '../model';
import { HostListener } from '@angular/core';
import { WordService } from '../services/word.service';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-learn-word-by-card',
  templateUrl: './learn-word-by-card.component.html',
  styleUrls: ['./learn-word-by-card.component.less']
})
export class LearnWordByCardComponent implements OnInit {
  words: Word[] = [];
  currentWordIndex: number = 0;
  isLoaded: boolean = false;
  isShowTranslate: boolean = false;
  isDone: boolean = false;
  isBackWay: boolean = false;
  typeOfLearn: TypeOfLearn;
  TypeOfLearnEnum = TypeOfLearn;

  constructor(private router: Router, private route: ActivatedRoute,
    private wordService: WordService) {
  }

  ngOnInit() {
    this.init();
  }

  init()
  {
    this.route.queryParams.subscribe((startLearnParams: StartLearnParams) => {
      let topicIds: Guid[] = [];
      if (typeof (startLearnParams.topicIds) === "string") {
        topicIds.push(Guid.parse(startLearnParams.topicIds));
      } else {
        topicIds=startLearnParams.topicIds;
      }
      this.typeOfLearn = Number(startLearnParams.typeOfLearn);

      this.wordService.getWordsByTopicIds(topicIds)
        .subscribe((data: Word[]) => {
          this.words = this.shuffleArray(data);
          this.isLoaded = true;
          if (this.typeOfLearn === TypeOfLearn.LearnBySound) {
            this.wordService.playAudio(this.words[0].voicePath);
          }
        });
    });
  }

  shuffleArray(array: Word[]): Word[] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  handleOk(): void {
    this.isDone = false;
    this.router.navigate(['/']);
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isDone = false;
  }

  handleAgain(): void {
    this.currentWordIndex = 0;
    this.words = this.shuffleArray(this.words);
    this.isDone = false;
  }

  handleAgainWithRevers(): void {
    this.handleAgain();
    this.isBackWay = !this.isBackWay;
  }

  playClick(): void {
    this.wordService.playAudio(this.words[this.currentWordIndex].voicePath);
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.keyCode === KEY_CODE.SPACE) {
      this.playClick();
      this.isShowTranslate = true;
      event.stopPropagation();
      return false;
    }

    if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
      this.isShowTranslate = false;
      if (this.currentWordIndex === this.words.length - 1) {
        this.isDone = true;
      }
      if (this.currentWordIndex < this.words.length - 1) {
        this.currentWordIndex++;
      }
      if (this.typeOfLearn === TypeOfLearn.LearnBySound) {
        this.playClick();
      }
    }

    if (event.keyCode === KEY_CODE.LEFT_ARROW) {
      this.isShowTranslate = false;
      if (this.currentWordIndex > 0) {
        this.currentWordIndex--;
      }
      if (this.typeOfLearn === TypeOfLearn.LearnBySound) {
        this.playClick();
      }
    }
  }
}
