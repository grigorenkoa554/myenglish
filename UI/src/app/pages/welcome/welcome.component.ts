import { Component, OnInit } from '@angular/core';
import { NzFormatEmitEvent, NzNotificationService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { PartOfSpeechType, StartLearnParams, Topic, TopicView, TypeOfLearn } from '../model';
import { TopicService } from '../services/topic.service';
import { Guid } from 'guid-typescript';
import { AccountService } from '../../account/account.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.less']
})
export class WelcomeComponent implements OnInit {
  public topics: TopicView[] = [];
  public error: string;
  public totalSelected: number = 0;
  public listNodes: any[] = [];

  constructor(private router: Router, private topicService: TopicService,
    private accountService: AccountService, private notification: NzNotificationService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.topicService.getAllView()
      .subscribe((data) => {
        const countOfTopic = this.accountService.getNickname() ? data.length : 3;
        this.topics = data.slice(0, countOfTopic);
        this.topics.forEach((topic: TopicView) => {
          let node = this.getNodes(topic);
          this.listNodes.push(node);
        });
      });
  }

  createBasicNotification(): void {
    if (!this.accountService.getNickname()) {
      this.notification
        .blank(
          'Dear user!',
          'If you want to get access to all themes, register on our website.'
        )
    }
  }

  defaultCheckedKeys = ['0-0-0'];
  defaultSelectedKeys = ['0-0-0'];
  defaultExpandedKeys = ['0-0', '0-0-0', '0-0-1'];

  getNodes(topic: TopicView): any {
    let nounCount = 0;
    let adjectiveCount = 0;
    let verbCount = 0;
    let item = topic.partOfSpeeches.find(x => x.partSpeech == PartOfSpeechType.Noun);
    if (item) {
      nounCount = item.count;
    }

    item = topic.partOfSpeeches.find(x => x.partSpeech == PartOfSpeechType.Adjective);
    if (item) {
      adjectiveCount = item.count;
    }
    item = topic.partOfSpeeches.find(x => x.partSpeech == PartOfSpeechType.Verb);
    if (item) {
      verbCount = item.count;
    }
    let nodes = [
      {
        title: 'All',
        key: '0-0',
        expanded: true,
        disabled: (nounCount + adjectiveCount + verbCount) === 0,
        children: [
          {
            title: `Noun (${nounCount})`, key: '0-0-0-0', isLeaf: true,
            disableCheckbox: nounCount === 0
          },
          {
            title: `Adjective (${adjectiveCount})`, key: '0-0-0-1', isLeaf: true,
            disableCheckbox: adjectiveCount === 0
          },
          {
            title: `Verb(${verbCount})`, key: '0-0-0-2', isLeaf: true,
            disableCheckbox: verbCount === 0
          }
        ]
      }
    ]
    return nodes;
  }

  nzEvent(event: NzFormatEmitEvent): void {
    console.log(event);
  }

  selectTopic(topic: Topic): void {
    topic.checked = !topic.checked;
    this.totalSelected = this.topics.map((topic: Topic) => {
      return topic.checked ? topic.totalCount : 0;
    }).reduce((a, b) => a + b, 0);
  }

  startLearnByText(): void {
    this.start(TypeOfLearn.LearnByText);
  }

  startLearnBySound(): void {
    this.start(TypeOfLearn.LearnBySound);
  }

  start(typeOfLearn: TypeOfLearn): void {
    let topicIds: Guid[] = this.topics.filter(topic => topic.checked)
      .map((topic: Topic) => topic.id);
    let params: StartLearnParams = {
      topicIds: topicIds,
      typeOfLearn: typeOfLearn
    };
    this.router.navigate(['/learnWordByCard'], { queryParams: params });
  }
}

