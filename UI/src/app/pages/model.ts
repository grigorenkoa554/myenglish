import { Guid } from 'guid-typescript';

export class StartLearnParams {
  topicIds: Guid[];
  typeOfLearn: TypeOfLearn;
}

export enum TypeOfLearn {
  LearnByText = 0,
  LearnBySound = 1
}


export class Topic {
  id: Guid;
  name: string;
  totalCount: number;
  checked?: boolean;

  words: Word[];
}

export class TopicView extends Topic {
  partOfSpeeches: PartOfSpeech[];
}

export class PartOfSpeech {
  topicId: Guid;
  partSpeech: number;
  count: number;
}

export class Word {
  id: Guid;
  wordEng: string;
  wordRus: string;
  partOfSpeech: PartOfSpeechType;
  topicId: Guid;
  voicePath: string;
}

export class WordView extends Word {
  isSelected: boolean;
}

export enum PartOfSpeechType {
  None = 0,
  Noun = 1,
  Adjective = 2,
  Verb = 3
}

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
  SPACE = 32
}
