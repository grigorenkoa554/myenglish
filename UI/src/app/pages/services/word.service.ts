import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { Word } from '../model';

@Injectable({
  providedIn: 'root'
})
export class WordService {
  host = '';
  constructor(private http: HttpClient) { }

  getWords(): Observable<Word[]> {
    return this.http.get<Word[]>(this.host + '/api/words/getall');
  }

  getWordsByTopicIds(topicIds: Guid[]): Observable<Word[]> {
    let params = new HttpParams();

    topicIds.forEach((id: Guid) => {
      params = params.append('topicIds', `${id}`);
    });

    return this.http.get<Word[]>(this.host + '/api/words/getWordsByTopicIds', { params });
  }

  addWord(topicId: Guid, wordEng: string, wordRus: string, partOfSpeech: number): Observable<Word> {
    const model = { topicId: topicId.toString(), wordEng, wordRus, partOfSpeech };
    return this.http.post<Word>(this.host + '/api/words/AddWord', model);
  }

  editWord(wordId: Guid, wordEng: string, wordRus: string, partOfSpeech: number
  ): Observable<string> {
    const model = { wordId: wordId.toString(), wordEng, wordRus, partOfSpeech };
    return this.http.put<string>(this.host + '/api/words/EditWord', model);
  }

  deleteWord(id: Guid): Observable<Word> {
    const model = { wordId: id.toString() };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: model
    };
    return this.http.delete<Word>(this.host + '/api/words/Delete/', httpOptions);
  }

  playAudio(filePath: string) {
    if (!filePath) {
      return;
    }
    let audio = new Audio();
    audio.src = this.host + '/' + filePath;
    audio.load();
    audio.play();
  }
}
