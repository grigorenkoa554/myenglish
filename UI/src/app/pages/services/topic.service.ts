import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { Topic, TopicView } from '../model';

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  host = '';

  constructor(private http: HttpClient) { }

  getTopics(): Observable<Topic[]> {
    return this.http.get<Topic[]>(this.host + '/api/topics/getall');
  }

  getAllView(): Observable<TopicView[]> {
    return this.http.get<TopicView[]>(this.host + '/api/topics/getAllView');
  }

  getTopic(topicId: Guid): Observable<Topic> {
    return this.http.get<Topic>(this.host + '/api/topics/getById/' + topicId);
  }

  addTopic(name: string): Observable<Topic> {
    const model = { name };
    return this.http.post<Topic>(this.host + '/api/topics/AddTopic', model);
  }

  editTopic(topicId: Guid, name: string): Observable<string> {
    const model = { topicId: topicId.toString(), name };
    return this.http.put<string>(this.host + '/api/topics/EditTopic', model);
  }

  countOfWordsNeedPartOfSpeech(topicId: Guid, partOfSpeech: number): Observable<number> {
    return this.http.get<number>(this.host + `/api/topics/countOfWordsNeedPartOfSpeech/${topicId}/${partOfSpeech}`);
  }

  deleteTopic(id: Guid): Observable<Topic> {
    const model = { topicId: id.toString() };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: model
    };
    return this.http.delete<Topic>(this.host + '/api/topics/Delete/', httpOptions);
  }
}
