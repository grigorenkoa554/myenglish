import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { WelcomeComponent } from '../app/pages/welcome/welcome.component';
import { UsersComponent } from './pages/users/users.component';
import { SomeEasyComponent } from './pages/some-easy/some-easy.component';
import { LearnWordByCardComponent } from './pages/learn-word-by-card/learn-word-by-card.component';
import { EditTopicsComponent } from './pages/edit/edit-topics/edit-topics.component';
import { EditTopicComponent } from './pages/edit/edit-topic/edit-topic.component';
import { UserComponent } from './pages/users/user/user.component';
import { BackItemComponent } from './common/back-item/back-item.component';

import { LoginComponent } from './account/account/login/login.component';
import { LogoutComponent } from './account/account/logout/logout.component';
import { RegisterComponent } from './account/account/register/register.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    UsersComponent,
    SomeEasyComponent,
    LearnWordByCardComponent,
    EditTopicsComponent,
    EditTopicComponent,
    UserComponent,
    BackItemComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  exports: [],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
