﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiMyEnglish.Models;

namespace WebApiMyEnglish.Repositories
{
    public interface IBaseRepository<T> : IDisposable
    {
        void Add(T t);
        void AddRange(IEnumerable<T> list);
        T GetById(Guid id);
        List<T> GetAll();
        void Delete(Guid id);
        void Update(T item);
    }
    public class BaseRepository<T> : IBaseRepository<T>, IDisposable where T : Base, new()
    {
        protected readonly MyEnglishDataContext dbContext;
        protected readonly DbSet<T> table;

        public BaseRepository(MyEnglishDataContext dbContext)
        {
            this.dbContext = dbContext;
            table = dbContext.Set<T>();
        }
        public void Add(T t)
        {
            table.Add(t);
            dbContext.SaveChanges();
        }

        public void AddRange(IEnumerable<T> list)
        {
            table.AddRange(list);
            dbContext.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var item = table.First(x => x.Id == id);
            table.Remove(item);
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            dbContext?.Dispose();
        }

        public List<T> GetAll()
        {
            return GetQuery().ToList();
        }

        public T GetById(Guid id)
        {
            var item = GetQuery().First(x => x.Id == id);
            return item;
        }

        public void Update(T item)
        {
            table.Update(item);
            dbContext.SaveChanges();
        }

        protected virtual IQueryable<T> GetQuery()
        {
            return table;
        }
    }
}
