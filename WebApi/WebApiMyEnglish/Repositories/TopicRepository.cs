﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApiMyEnglish.Models;

namespace WebApiMyEnglish.Repositories
{
	public interface ITopicRepository : IDisposable, IBaseRepository<Topic>
	{
		ITopicRepository Join(TopicJoinEnum topicJoin);
		IEnumerable<TopicView> GetTopicViews();
	}

	public class TopicRepository : BaseRepository<Topic>, ITopicRepository
	{
		private TopicJoinEnum topicJoin;

		public TopicRepository(MyEnglishDataContext dbContext) : base(dbContext)
		{
		}

		public IEnumerable<TopicView> GetTopicViews()
		{
			var groups = dbContext.Words.GroupBy(word => new { word.TopicId, word.PartOfSpeech },
				 (key, groups) => new PartOfSpeech
				 {
					 TopicId = key.TopicId,
					 PartSpeech = key.PartOfSpeech,
					 Count = groups.Count()
				 });

			var topics = dbContext.Topics.ToList();

			var viewTopics = new List<TopicView>();
			foreach (var topic in topics)
			{
				var some2 = groups.Where(x => x.TopicId == topic.Id);
				viewTopics.Add(new TopicView
				{
					Id = topic.Id,
					Name = topic.Name,
					TotalCount = topic.TotalCount,
					PartOfSpeeches = some2
				});
			}

			return viewTopics;
		}

		public ITopicRepository Join(TopicJoinEnum topicJoin)
		{
			this.topicJoin = topicJoin;
			return this;
		}

		protected override IQueryable<Topic> GetQuery()
		{
			IQueryable<Topic> querry = table;

			if (topicJoin == TopicJoinEnum.Word)
			{
				querry = querry.Include(x => x.Words);
			}

			return querry;
		}
	}

	public enum TopicJoinEnum
	{
		None = 0,
		Word = 1
	}

	public class TopicView : Topic
	{
		public IEnumerable<PartOfSpeech> PartOfSpeeches { get; set; }
	}

	public class PartOfSpeech
	{
		public Guid TopicId { get; set; }
		public int PartSpeech { get; set; }
		public int Count { get; set; }
	}
}
