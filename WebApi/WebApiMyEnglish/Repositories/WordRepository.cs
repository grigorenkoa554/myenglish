﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApiMyEnglish.Models;

namespace WebApiMyEnglish.Repositories
{
	public interface IWordRepository
	{
		List<Word> GetAll();
		List<Word> GetBy(List<Guid> topicIds);
		Guid Add(Word word);
		void Delete(Guid id);
		void Edit(Guid id, Word word);
		Word Get(Guid id);
	}

	public class DbWordRepository : IWordRepository
	{
		private readonly MyEnglishDataContext myEnglishDataContext;

		public DbWordRepository(MyEnglishDataContext myEnglishDataContext)
		{
			this.myEnglishDataContext = myEnglishDataContext;
		}
		public List<Word> GetAll()
		{
			return myEnglishDataContext.Words.ToList();
		}

		public List<Word> GetBy(List<Guid> topicIds)
		{
			return myEnglishDataContext.Words
				.Where(word => topicIds.Any(topicId => topicId == word.TopicId))
				.ToList();
		}

		public Guid Add(Word word)
		{
			throw new NotImplementedException();
		}

		public void Delete(Guid id)
		{
			Word word = myEnglishDataContext.Words.ToList().Single(x => x.Id == id);
			myEnglishDataContext.Words.Remove(word);
			myEnglishDataContext.SaveChanges();
		}

		public void Edit(Guid id, Word word)
		{
			throw new NotImplementedException();
		}

		public Word Get(Guid id)
		{
			throw new NotImplementedException();
		}
	}
}
