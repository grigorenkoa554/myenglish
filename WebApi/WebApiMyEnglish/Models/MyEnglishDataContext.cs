﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace WebApiMyEnglish.Models
{
    public class MyEnglishDataContext : IdentityDbContext<User>
    {
        public MyEnglishDataContext(DbContextOptions<MyEnglishDataContext> 
            options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Word> Words { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            AddAdmin(modelBuilder);
        }

        private void AddAdmin(ModelBuilder builder)
        {
            string ADMIN_ID = "8069F4FD-E2AE-4B53-9BC9-171582C42F0E";
            string ADMIN_ROLE_ID = "4C58C18B-2A8E-4171-BD64-F172F5C05CF3";
            string USER_ROLE_ID = "E65E806B-4739-45BA-A4E4-A584279AC4BD";

            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Name = "Admin",
                NormalizedName = "Admin",
                Id = ADMIN_ROLE_ID,
                ConcurrencyStamp = ADMIN_ROLE_ID
            });
            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Name = "User",
                NormalizedName = "User",
                Id = USER_ROLE_ID,
                ConcurrencyStamp = USER_ROLE_ID
            });

            var appUser = new User
            {
                Id = ADMIN_ID,
                Email = "admin@admin.admin",
                NormalizedEmail = "admin@admin.admin",
                EmailConfirmed = true,
                UserName = "admin@admin.admin",
                NormalizedUserName = "admin@admin.admin",
            };

            PasswordHasher<User> ph = new PasswordHasher<User>();
            appUser.PasswordHash = ph.HashPassword(appUser, "2");

            builder.Entity<User>().HasData(appUser);

            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = ADMIN_ROLE_ID,
                UserId = ADMIN_ID
            });
        }
    }
}