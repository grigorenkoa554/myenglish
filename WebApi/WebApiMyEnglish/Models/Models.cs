﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiMyEnglish.Models
{
    public class Base
    {
        public Guid Id { get; set; }
    }

    [Table("Topic")]
    public class Topic : Base
    {
        public string Name { get; set; }
        public int TotalCount { get; set; }

        public ICollection<Word> Words { get; set; }
    }

    [Table("Word")]
    public class Word : Base
    {
        public string WordEng { get; set; }
        public string WordRus { get; set; }
        public int PartOfSpeech { get; set; }
        public string VoicePath { get; set; }

        public Guid TopicId { get; set; }
        public Topic Topic { get; set; }
    }

    public class User : IdentityUser
    {
        public string Nickname { get; set; }
    }

    public class UserResource : Base
    {
        public Guid UserId { get; set; }
    }
}
