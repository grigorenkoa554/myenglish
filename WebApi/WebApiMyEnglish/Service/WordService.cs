﻿using FunApi.Services.Import;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebApiMyEnglish.Models;
using WebApiMyEnglish.Repositories;

namespace WebApiMyEnglish.Service
{
    public interface IWordService
    {
        Word AddWord(AddWordModel model);
        void DeleteWord(DeleteWordModel model);
        void EditWord(EditWordModel model);
        void UpdateSoundAllWords();
    }
    public class WordService : IWordService
    {
        private readonly IBaseRepository<Word> baseRepository;
        private readonly IBaseRepository<Topic> baseRepositoryForTopic;
        private readonly IAudioService audioService;
        private static object lockObject = new object();

        public WordService(IBaseRepository<Word> baseRepository,
            IBaseRepository<Topic> baseRepositoryForTopic, IAudioService audioService)
        {
            this.baseRepository = baseRepository;
            this.baseRepositoryForTopic = baseRepositoryForTopic;
            this.audioService = audioService;
        }
        public Word AddWord(AddWordModel model)
        {
            var webPath = audioService.AddAudioPath(model.WordEng).Url;
            var word = new Word
            {
                WordEng = model.WordEng,
                WordRus = model.WordRus,
                PartOfSpeech = model.PartOfSpeech,
                TopicId = model.TopicId,
                VoicePath = webPath
            };

            baseRepository.Add(word);
            var topic = baseRepositoryForTopic.GetById(model.TopicId);
            topic.TotalCount = topic.TotalCount + 1;
            baseRepositoryForTopic.Update(topic);
            return word;
        }

        public void DeleteWord(DeleteWordModel model)
        {
            lock (lockObject)
            {
                var word = baseRepository.GetById(model.WordId);
                var count = baseRepository.GetAll().Where(x => x.TopicId == word.TopicId).Count();
                var topic = baseRepositoryForTopic.GetById(word.TopicId);
                topic.TotalCount = count;
                baseRepository.Delete(model.WordId);
                topic.TotalCount--;
                baseRepositoryForTopic.Update(topic);
            }
        }

        public void EditWord(EditWordModel model)
        {
            var webPath = audioService.AddAudioPath(model.WordEng).Url;
            var word = baseRepository.GetById(model.WordId);
            word.WordEng = model.WordEng;
            word.WordRus = model.WordRus;
            word.VoicePath = webPath;
            word.PartOfSpeech = model.PartOfSpeech;
            baseRepository.Update(word);
        }

        public void UpdateSoundAllWords()
        {
            var words = baseRepository.GetAll();
            foreach (var word in words)
            {
                var webPath = audioService.AddAudioPath(word.WordEng).Url;
                word.VoicePath = webPath;
                baseRepository.Update(word);
            }
        }
    }
    public class AddWordModel
    {
        [Required]
        public Guid TopicId { get; set; }
        [Required]
        public string WordEng { get; set; }
        [Required]
        public string WordRus { get; set; }
        public int PartOfSpeech { get; set; }
    }

    public class DeleteWordModel
    {
        [Required]
        public Guid WordId { get; set; }
    }

    public class EditWordModel
    {
        [Required]
        public Guid WordId { get; set; }
        [Required]
        public string WordEng { get; set; }
        [Required]
        public string WordRus { get; set; }
        public int PartOfSpeech { get; set; }
    }
}
