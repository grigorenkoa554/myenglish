﻿using System.IO;

namespace FunApi.Services
{
	public interface ISettingService
	{
		string AssetsFolderName { get; }
		string SourceRoot { get; }
		string ResourceGrammarFolder { get; }
		string ResourceDictionaryFolder { get; }
		string ResourceSoundFolder { get; }
		string ResourceSoundFolderWebApi { get; }
	}

	public class SettingService : ISettingService
	{
		public string AssetsFolderName
		{
			get
			{
				return @"assets";
			}
		}
		public string SourceRoot
		{
			get
			{
				return @"C:\killMe\FunApi\FunApi\ClientApp\src";
			}
		}
		public string ResourceSoundFolderWebApi
		{
			get
			{
				return @"api/resources/sounds";
			}
		}
		public string ResourceSoundFolder
		{
			get
			{
				return Path.Combine(Directory.GetCurrentDirectory(), @"Resources/Sounds");
			}
		}
		public string ResourceDictionaryFolder
		{
			get
			{
				return Path.Combine(Directory.GetCurrentDirectory(), @"ResourcesInternal/Dictionary");
			}
		}
		public string ResourceGrammarFolder
		{
			get
			{
				return Path.Combine(Directory.GetCurrentDirectory(), @"ResourcesInternal/Grammar");
			}
		}
	}
}
