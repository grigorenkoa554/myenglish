﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApiMyEnglish.Service
{
    public interface IAuthService
    {
        Guid CurrentUserId { get; }
    }

    public class AuthService : IAuthService
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private Guid currentUserId;

        public AuthService(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public Guid CurrentUserId
        {
            get
            {
                if (currentUserId == default)
                {
                    var str = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                    Guid.TryParse(str, out currentUserId);
                }
                return currentUserId;
            }
        }
    }
}
