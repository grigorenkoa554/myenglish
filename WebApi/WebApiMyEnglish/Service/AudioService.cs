﻿using System.IO;
using System.Text.RegularExpressions;
using Google.Cloud.TextToSpeech.V1;

namespace FunApi.Services.Import
{
	public interface IAudioService
	{
		FileNameResult AddAudioPath(string textToSpeech);
	}

	public class AudioService : IAudioService
	{
		private readonly ISettingService setting;


		public AudioService(ISettingService setting)
		{
			this.setting = setting;
		}

		public FileNameResult AddAudioPath(string textToSpeech)
		{
			var voiceNameCode = "en-US-Wavenet-C";
			var pitch = 1.0;
			var speakingRate = 1.0;
			var ext = "mp3";

			var fileName = GetFileName(textToSpeech, voiceNameCode, pitch, speakingRate, ext); // i_have_got-1_0-1_0.mp3
			/// @"C:\killMe\FunApi\FunApi\ClientApp\src\assets\en-US-Wavenet-C\i_have_got-1_0-1_0.mp3"
			var filePath = $@"{setting.ResourceSoundFolder}\{voiceNameCode}\{fileName}";

			var isExists = new FileInfo(filePath).Exists;
			if (isExists)
			{
				return new FileNameResult
				{
					Url = $@"{setting.ResourceSoundFolderWebApi}\{voiceNameCode}\{fileName}",
					Status = Status.NotCreatedAlreadyExists
				};
			}

			var client = TextToSpeechClient.Create();

			// Set the text input to be synthesized.
			var input = new SynthesisInput
			{
				//Text = "Hello, World!"
				//Text = @"I have got."
				Text = textToSpeech
			};

			// Build the voice request, select the language code ("en-US"),
			// and the SSML voice gender ("neutral").
			var voice = new VoiceSelectionParams
			{
				LanguageCode = "en-US",
				SsmlGender = SsmlVoiceGender.Neutral,
				Name = voiceNameCode
			};

			// Select the type of audio file you want returned.
			var config = new AudioConfig
			{
				AudioEncoding = AudioEncoding.Mp3,
				Pitch = pitch,
				SpeakingRate = speakingRate
			};

			// Perform the Text-to-Speech request, passing the text input
			// with the selected voice parameters and audio file type
			var response = client.SynthesizeSpeech(new SynthesizeSpeechRequest
			{
				Input = input,
				Voice = voice,
				AudioConfig = config
			});

			var fileInfo = new FileInfo(filePath);
            if (!fileInfo.Directory.Exists)
            {
				fileInfo.Directory.Create();
            }
			// Write the binary AudioContent of the response to an MP3 file.
			using (Stream output = File.Create(filePath))
			{
				response.AudioContent.WriteTo(output);
			}

			return new FileNameResult
			{
				Url = $@"{setting.ResourceSoundFolderWebApi}\{voiceNameCode}\{fileName}",
				Status = Status.Created
			};
		}

		private string GetFileName(string textToSpeech, string voiceNameCode, double pitch, double speakingRate,
			string ext)
		{
			var fileName = $"{Clean(textToSpeech)}-{Clean(voiceNameCode)}-{Clean(pitch)}-{Clean(speakingRate)}.{ext}";
			return fileName;
		}

		private string Clean(double number)
		{
			return Clean(number.ToString());
		}

		private string Clean(string text)
		{
			var rgx = new Regex("[^a-zA-Z0-9]");
			var result = rgx.Replace(text, "_");
			return result;
		}
	}

	public class FileNameResult
	{
		public string Url { get; set; }
		public Status Status { get; set; }
	}

	public enum Status
	{
		Created = 1,
		NotCreatedAlreadyExists = 2
	}
}