﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApiMyEnglish.Models;
using WebApiMyEnglish.Repositories;

namespace WebApiMyEnglish.Service
{
    public interface ITopicService
    {
        bool ExistOrNonExistId(Guid id);
        Topic AddTopic(AddTopicModel model);
        void EditTopic(EditTopicModel model);
        int CountOfWordsWithPartOfSpeech(Guid topicId, int partOfSpeech);
        void DeleteTopic(DeleteTopicModel model);
    }
    public class TopicService : ITopicService
    {
        private readonly IBaseRepository<Topic> baseRepository;
        private readonly IBaseRepository<Word> baseRepositoryForWords;
        public TopicService(IBaseRepository<Topic> baseRepository,
            IBaseRepository<Word> baseRepositoryForWords)
        {
            this.baseRepository = baseRepository;
            this.baseRepositoryForWords = baseRepositoryForWords;
        }

        public int CountOfWordsWithPartOfSpeech(Guid topicId, int partOfSpeech)
        {
            var topic = baseRepository.GetById(topicId);
            var words = baseRepositoryForWords.GetAll().Where(x =>
                x.TopicId == topicId && x.PartOfSpeech == partOfSpeech);
            var count = words.Count();
            return count;
        }

        public Topic AddTopic(AddTopicModel model)
        {
            var topic = new Topic { Name = model.Name };
            baseRepository.Add(topic);
            return topic;
        }

        public void EditTopic(EditTopicModel model)
        {
            var topic = baseRepository.GetById(model.TopicId);
            topic.Name = model.Name;
            baseRepository.Update(topic);
        }

        public bool ExistOrNonExistId(Guid id)
        {
            var list = baseRepository.GetAll();
            if (list.Exists(x => x.Id == id))
            {
                return true;
            }
            return false;
        }
        public void DeleteTopic(DeleteTopicModel model)
        {
            baseRepository.Delete(model.TopicId);
        }
    }

    public class AddTopicModel
    {
        [Required]
        public string Name { get; set; }
    }

    public class EditTopicModel
    {
        [Required]
        public Guid TopicId { get; set; }
        [Required]
        public string Name { get; set; }
    }
    public class DeleteTopicModel
    {
        [Required]
        public Guid TopicId { get; set; }
    }

}
