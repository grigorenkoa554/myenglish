﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiMyEnglish.Models;
using WebApiMyEnglish.Repositories;
using WebApiMyEnglish.Service;

namespace WebApiMyEnglish.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TopicsController : ControllerBase
    {
        private readonly ITopicRepository topicRepository;
        private readonly ITopicService topicService;

        public TopicsController(ITopicRepository topicRepository, ITopicService topicService)
        {
            this.topicRepository = topicRepository;
            this.topicService = topicService;
        }
        
        [HttpGet]
        public IEnumerable<Topic> GetAll()
        {
            using (topicRepository)
            {
                return topicRepository.GetAll();
            }
        }

        [HttpGet]
        public IEnumerable<TopicView> GetAllView()
        {
            {
                //if (Roles.IsUserInRole(User.Identity.Name, "Admin") || 
                //    Roles.IsUserInRole(User.Identity.Name, "User"))
				{
                    return topicRepository.GetTopicViews();

				}
            }
        }

        [HttpGet("{id}")]
        public Topic GetById(Guid id)
        {
            using (topicRepository)
            {
                return topicRepository.Join(TopicJoinEnum.Word).GetById(id);
            }
        }

        [HttpGet("{topicId}/{partOfSpeech}")]
        public int CountOfWordsNeedPartOfSpeech(Guid topicId, int partOfSpeech)
        {
            return topicService.CountOfWordsWithPartOfSpeech(topicId, partOfSpeech);
        }


        [HttpGet("{id}")]
        public Topic GetByIdIfIdExist(Guid id)
        {
            using (topicRepository)
            {
                if (topicService.ExistOrNonExistId(id))
                {
                    return topicRepository.GetById(id);
                }
                return new Topic { Name = "", TotalCount = 0 };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public Topic AddTopic(AddTopicModel model)
        {
            return topicService.AddTopic(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public object EditTopic(EditTopicModel model)
        {
            topicService.EditTopic(model);
            return new { res = "ok! topic was edited" };
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public void Delete(DeleteTopicModel model)
        {
            topicService.DeleteTopic(model);
        }
    }
}
