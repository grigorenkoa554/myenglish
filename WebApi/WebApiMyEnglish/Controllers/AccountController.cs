﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApiMyEnglish.Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiMyEnglish.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [HttpPost("[action]")]
        public async Task<object> Register(RegisterModel model)
        {
            return await accountService.Register(model);
        }

        [HttpPost("[action]")]
        public async Task<object> Login(LoginModel model)
        {
            var nickname = await accountService.Login(model);
            return new { nickname = nickname };
        }

        [HttpPost("[action]")]
        public async Task<object> Logout()
        {
            return await accountService.Logout();
        }
    }
}
