﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiMyEnglish.Models;
using WebApiMyEnglish.Repositories;
using WebApiMyEnglish.Service;

namespace WebApiMyEnglish.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WordsController : ControllerBase
    {
        private IWordRepository wordRepository;
        private readonly IBaseRepository<Word> baseRepository;
        private readonly IWordService wordService;
        public WordsController(IBaseRepository<Word> baseRepository,
            IWordRepository wordRepository,
            IWordService wordService)
        {
            this.baseRepository = baseRepository;
            this.wordRepository = wordRepository;
			this.wordService = wordService;
        }

        [HttpGet]
        public IEnumerable<Word> GetAll()
        {
            using (baseRepository)
            {
                return baseRepository.GetAll();
            }
        }

        [HttpGet("{id}")]
        public Word GetById(Guid id)
        {
            using (baseRepository)
            {
                return baseRepository.GetById(id);
            }
        }

        [HttpGet()]
        public IEnumerable<Word> GetWordsByTopicIds([FromQuery(Name = "topicIds")] List<Guid> topicIds)
        {
            return wordRepository.GetBy(topicIds);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public Word AddWord(AddWordModel model)
        {
            return wordService.AddWord(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public object EditWord(EditWordModel model)
        {
            wordService.EditWord(model);
            return new { res = "Word was edit" };
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public object UpdateSoundAllWords()
        {
            wordService.UpdateSoundAllWords();
            return new { res = "all words sond were updated." };
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public void Delete(DeleteWordModel model)
        {
            wordService.DeleteWord(model);
        }
    }
}
