﻿using FunApi.Services;
using FunApi.Services.Import;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System.IO;
using WebApiMyEnglish.Models;
using WebApiMyEnglish.Repositories;
using WebApiMyEnglish.Service;

namespace WebApiMyEnglish
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "MyAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<MyEnglishDataContext>(options => options.UseSqlServer(connection));

            services.AddIdentity<User, IdentityRole>(
                    options =>
                    {
                        options.Password.RequireLowercase = false;
                        options.Password.RequireUppercase = false;
                        options.Password.RequireDigit = false;
                        options.Password.RequiredLength = 1;
                        options.Password.RequireNonAlphanumeric = false;
                    })
                .AddEntityFrameworkStores<MyEnglishDataContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IBaseRepository<Topic>, BaseRepository<Topic>>();
            services.AddTransient<IBaseRepository<Word>, BaseRepository<Word>>();
            services.AddTransient<ITopicService, TopicService>();
            services.AddTransient<IAudioService, AudioService>();
            services.AddTransient<IWordService, WordService>();
            services.AddTransient<ISettingService, SettingService>();
            services.AddTransient<ITopicRepository, TopicRepository>();
            services.AddTransient<IWordRepository, DbWordRepository>();
            services.AddTransient<IAccountService, AccountService>();

            services.AddMvc()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            services.AddSwaggerGen(x => { x.SwaggerDoc("v1", new OpenApiInfo { Title = "My Api!", Version = "V1" }); });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                   !Path.HasExtension(context.Request.Path.Value) &&
                   !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();

			app.UseStaticFiles(new StaticFileOptions
			{
				FileProvider = new PhysicalFileProvider(
					Path.Combine(env.ContentRootPath, "Resources")),
				RequestPath = "/api/Resources"
			});

			app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
